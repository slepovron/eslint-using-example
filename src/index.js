// place for your bad code =)

const b = 5;
function c(a, b) {
  return a + b;
}
console.log(c());

let a = 2;
function v(name) {
  return name;
}

console.log(v("Fedor"), a);

// eslint-disable-next-line no-console
console.log(window);
console.log(document);

a++;

const arrow = () => {
  const b = "bad code";
  console.log(b);
};

const object = {
  key: "Key",
};

// error alert if you don't use destructuring
const { key } = object;
